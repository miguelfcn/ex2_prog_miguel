package utfpr.dainf.ct.ed.exemplo;

/**
 * UTFPR - Universidade Tecnológica Federal do Paraná
 * DAINF - Departamento Acadêmico de Informática
 * 
 * Exemplo de implementação de árvore binária de pesquisa.
 * @author Wilson Horstmeyer Bogado <wilson@utfpr.edu.br>
 * @param <E> O tipo do valor armazenado nos nós da árvore
 */
public class ArvoreBinariaPesquisa<E extends Comparable<E>> extends ArvoreBinaria<E> {
    protected ArvoreBinariaPesquisa<E> pai;

    /**
     * Cria uma árvore com valor nulo na raiz.
     */
    public ArvoreBinariaPesquisa() {
    }

    /**
     * Cria uma árvore com o valor especificado na raiz.
     * @param valor O valor armazenado na raiz.
     */
    public ArvoreBinariaPesquisa(E valor) {
        super(valor);
    }

    /**
     * Inicializa o nó pai deste nó.
     * @param pai O nó pai.
     */
    protected void setPai(ArvoreBinariaPesquisa<E> pai) {
        this.pai = pai;
    }

    /**
     * Retorna o nó pai deste nó.
     * @return O nó pai.
     */
    protected ArvoreBinariaPesquisa<E> getPai() {
        return pai;
    }

    /**
     * Retorna o nó da árvore cujo valor corresponde ao especificado.
     * @param valor O valor a ser localizado.
     * @return A raiz da subárvore contendo o valor ou {@code null}.
     */
    public ArvoreBinariaPesquisa<E> pesquisa(E valor) {
        ArvoreBinariaPesquisa<E> aux=this;
        
        while(aux.getValor()!= valor)
        {
            if(aux.getValor().compareTo(valor) > 0 )
                aux=(ArvoreBinariaPesquisa)aux.esquerda;
         
            else 
                aux=(ArvoreBinariaPesquisa)aux.direita;
            
            if (aux == null)
                break;
        }

        return aux;
    }

    /**
     * Retorna o nó da árvore com o menor valor.
     * @return A raiz da subárvore contendo o valor mínimo
     */
    public ArvoreBinariaPesquisa<E> getMinimo() { 
        ArvoreBinariaPesquisa<E> aux=this;
        while (aux.esquerda!=null)
            aux=(ArvoreBinariaPesquisa<E>)aux.esquerda;
        return aux;
    }
   

    /**
     * Retorna o nó da árvore com o maior valor.
     * @return A raiz da subárvore contendo o valor máximo
     */
    public ArvoreBinariaPesquisa<E> getMaximo() {
        ArvoreBinariaPesquisa<E> aux=this;
        while (aux.direita!=null)
            aux=(ArvoreBinariaPesquisa<E>)aux.direita;
        return aux;
    }

    /**
     * Retorna o nó sucessor do nó especificado.
     * @param no O nó cujo sucessor desejamos localizar
     * @return O sucessor do no ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> sucessor(ArvoreBinariaPesquisa<E> no) {
        if(no!= null)
    {
            if( no.direita!= null)
        {
            no=(ArvoreBinariaPesquisa<E>)no.direita;
            no=no.getMinimo();
            return no;
        }
       
                ArvoreBinariaPesquisa<E> p=no.getPai();
             
                while(p!= null && no==p.direita)
            {
                no=p;
                p=p.getPai();
            }
            return p;
    }
        return null;
    }

    /**
     * Retorna o nó predecessor do nó especificado.
     * @param no O nó cujo predecessor desejamos localizar
     * @return O predecessor do nó ou {@null}.
     */
    public ArvoreBinariaPesquisa<E> predecessor(ArvoreBinariaPesquisa<E> no) {
        if(no!= null)
        {
                    if( no.esquerda!=null)
                {
                    no=(ArvoreBinariaPesquisa<E>)no.esquerda;
                    no=no.getMaximo();
                    return no;
                }
                   
                    else if(no.esquerda==null)
                {
                    ArvoreBinariaPesquisa<E> p=no.getPai();

                    while(p!= null && no==p.esquerda)
                    {
                        no=p;
                        p=p.getPai();
                    }
                    return p;
                }
        }
    return null;
    }

    /**
     * Insere um nó contendo o valor especificado na árvore.
     * @param valor O valor armazenado no nó.
     * @return O nó inserido
     */
    public ArvoreBinariaPesquisa<E> insere(E valor) {
          ArvoreBinariaPesquisa<E> aux2= new ArvoreBinariaPesquisa<>(valor);
          ArvoreBinariaPesquisa<E> aux=this;     
             if(aux.getValor().compareTo(valor)>0)
             {  
                 if(aux.esquerda==null)
                     aux.esquerda= aux2;
                else
                 {  aux=(ArvoreBinariaPesquisa<E>)aux.esquerda;
                     aux.insere(valor);
                 }
             }
             else
             {  
                 if(aux.direita==null)
                     aux.direita= aux2;
                else
                 {  aux=(ArvoreBinariaPesquisa<E>)aux.direita;
                     aux.insere(valor);
                 }
             }
 
         return aux;
    }

    /**
     * Exclui o nó especificado da árvore.
     * Se a raiz for excluída, retorna a nova raiz.
     * @param no O nó a ser excluído.
     * @return A raiz da árvore
     */
    public ArvoreBinariaPesquisa<E> exclui(ArvoreBinariaPesquisa<E> no) {
                    ArvoreBinariaPesquisa<E> aux2;

        if(this!= no){
        return null;
        }
       else if(this == no)
       {
           return null;
       }
       return null;
    }
}
